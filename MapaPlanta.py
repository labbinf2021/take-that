#!/usr/bin/env python3
import numpy as np
import pandas as pd
import seaborn as sns
import csv
from osgeo import gdal
import folium

def csv_cleaner(csv_df):
    """
    Cleans csv from unecessary data
    Takes a Panda df as an input
    Returns a (cleaned) Pandas dfif __name__ == "__main__":
    LOCATION= pd.read_csv('planta.csv', names = ['id', 'latitude', 'longitude'], sep=';')
    CLEAN_LOCATION = csv_cleaner(LOCATION)
    """
    csv_df.dropna(axis=0, how="any", inplace=True)

    return csv_df


if __name__ == "__main__":
    LOCATION= pd.read_csv('planta.csv', names = ['id', 'latitude', 'longitude'], sep=';')
    CLEAN_LOCATION = csv_cleaner(LOCATION)


def map_maker(csv_df):
    csv_df= csv_df.iloc[1: , :]
    mapa= folium.Map(
    location=[11.7679957855,11.7679957855],
    zoom_start=2
    )
    for _, city in csv_df.iterrows():
        folium.Marker(
                location=[city['latitude'], city['longitude']]

    
    
    ).add_to(mapa)
    mapa.save("mapa.html")

csv_cleaner(CLEAN_LOCATION)
map_maker(CLEAN_LOCATION)