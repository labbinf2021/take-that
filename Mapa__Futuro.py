pip install plotly

import plotly.graph_objects as go
import pandas as pd

data = pd.read_csv("climate change.csv", sep = ";")
data.head()

data.drop('Sample', axis = 1, inplace = True)
data.head()

dataplants = pd.read_csv("planta.csv", sep = ";")
dataplants.head()

df = dataplants.join(data)
df.head()

import os

if not os.path.exists("images"):
    os.mkdir("images")

import plotly.express as px
fig = px.density_mapbox(df, lat='latitude', lon='longitude', radius=10,
                        center=dict(lat=39, lon=18), zoom=3,
                        mapbox_style="stamen-terrain")
fig.show()

fig.write_html("images/fig2.html")