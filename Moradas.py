#importação de bibliotecas
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

#importar os valoresv dos tifs
data = pd.read_csv("valores ML.csv", sep = ";")
data.head()

#eliminação da coluna sample
data.drop('Sample', axis = 1, inplace = True)
data.head()

#importar o dtaset da localização das plantas
dataplants = pd.read_csv("planta.csv", sep = ";")
dataplants.head()

df2=dataplants.drop(columns=['id'], axis=1)
df2.head()

lats=df2['latitude'].tolist()
lons=df2['longitude'].tolist()

coords=list(zip(lats,lons))

from geopy.geocoders import Nominatim
geolocator = Nominatim(user_agent="test_app")  # enter a name for your app
full_address=[]
for i in range(len(coords)):
    location = geolocator.reverse(coords[i])
    address=location.raw['display_name']
    full_address.append(address)

add=pd.DataFrame(data=full_address , columns=['Address'])
add

df3 = data.join(add)

df3.head(100)

from sklearn.preprocessing import LabelEncoder

# creating instance of labelencoder
labelencoder = LabelEncoder()
# Assigning numerical values and storing in another column
df3['Address_Cat'] = labelencoder.fit_transform(df3['Address'])
df3


from sklearn.model_selection import train_test_split

x = df3.drop(['Address', 'Address_Cat' ], axis=1)
y = df3['Address_Cat']

x_train, x_test, y_train, y_test = train_test_split (x,y, test_size = 0.30)


from sklearn.ensemble import RandomForestRegressor
random_forest = RandomForestRegressor()

random_forest.fit(x_train, y_train)

random_forest.predict(x_test)

prevision = random_forest.predict(x_test)
accuracy = random_forest.score(x_test, y_test)

print('accuracy = ', accuracy)