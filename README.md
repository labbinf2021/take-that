# Take That

Second chance

## Getting Things up and running

1st step: Run the MapaPlanta.py however it is needed that you have the planta.csv in the same directory as the MapaPlanta.py

*2nd step: Run the lb.py script having (just like step one) all the .tiff files in the same directory. This scrpit will need you to name the csv that it has to generate, for exemple the wc2.1_2.5m_bio_1.tif will generate a csv that contains the Anual Medium Temperature, so we'll call him "Anual Medium Temperature", this process has to be done to all of the tif files

*Note: Our group assembled all the generated csv files into these main 2: valores ML.csv wich contains all the generated files of the present bioclimatics variables, and the climate change.csv contains all the generated csv's of the bioclimatic of the future

3rd step: Run the Mapa_Presente.py, having in the same directory the following csv file: the valores ML.csv  
4th step: Run the Mapa__Futuro, having in the same directory the following csv file: climate change.csv

5th step: Run the teste_statistic_2.0.py, for this script it is mandatory to have both of climate change.csv and valores Ml.csv in the same directory

*Note: To easy all of this process, create a new directory or a new folder containig all these scripts and csv's, in this way all of them are already in the same directory.





