from collections import OrderedDict
from osgeo import gdal


def get_value_from_point(rasterfile, coords):
    """
    Open a rasterfile, read the data on the coordinates 'pos',
    and return that value.
    """
    gisdata = gdal.Open(rasterfile)
    trnsf = gisdata.GetGeoTransform()
    data = gisdata.GetRasterBand(1)
    intvals = []

    for sample, crds in coords.items():
        x_coord = int((crds[0] - trnsf[0]) / trnsf[1])
        y_coord = int((crds[1] - trnsf[3]) / trnsf[5])

        intval = data.ReadAsArray(x_coord, y_coord, 1, 1)
        print('{}\t{}'.format(sample, intval[0][0]))
        intvals.append((sample, intval[0][0]))
    return intvals


def read_shapefile(shapefile_name):
    """Read the shapefile and return an OrderedDict {LABEL: (LON, LAT)}."""
    shapes = open(shapefile_name, 'r')
    shapes.readline()  # Skip header
    coords = OrderedDict()
    for lines in shapes:
        lines = lines.split(";")
        coords[lines[0]] = tuple(map(float, lines[1:3][::-1]))

    shapes.close()
    return coords


if __name__ == "__main__":
    # Usage: python3 layer_data_extractor.py shapefile.csv rasterfile(s)
    # shapefile.csv should have a header and the following data: LABEL LAT LON
    from sys import argv
    COORDS = read_shapefile(argv[1])
    print("Sample\t" + "\t".join(argv[2:]))
    intvals = get_value_from_point(argv[2], COORDS)
    with open ("precipitacaocoefdevariacao.csv", "w" ) as csv:
        for intval in intvals:
            csv.write(str(intval[0])+";"+str(intval[1])+"\n")


        