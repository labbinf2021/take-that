#!/usr/bin/env python3
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import pingouin as pg
import scipy as sp
from sklearn.preprocessing import LabelEncoder as le
from scipy import stats


def treino_stat():
    #Cleaning the data 
    data_p = pd.read_csv("valores ML.csv", sep = ";")
    data_p.drop('Sample', axis = 1, inplace = True)

    data_f = pd.read_csv("climate change.csv", sep = ";")
    data_f.drop('Sample', axis = 1, inplace = True)



#Defining which data set is "from the future" and the "present"

#Present
    data_p['Temporal state'] = 'Present'
    data_p['Temporal state']= data_p['Temporal state'].astype(str)
    data_p['Temperatura Máxima']=data_p['Temperatura Máxima'].astype(int)


#Future
    data_f['Temporal state'] = 'Future'
    data_f['Temporal state']= data_f['Temporal state'].astype(str)
    data_f['Temperatura max futura']=data_f['Temperatura max futura'].astype(int)

#subdivinding both datasets

    presente_ = data_p[(data_p['Temporal state'] == 'Temperatura Máxima')]
    futuro_ = data_f[(data_f['Temporal state'] == 'Temperatura max futura')]


    #returning and showing the results of the statistic test
    return sp.stats.ttest_ind(presente_.dropna()['Temperatura Máxima'], futuro_.dropna()['Temperatura max futura'])





treino_stat()
print(sp.stats.ttest_ind(presente_.dropna()['Temperatura Máxima'], futuro_.dropna()['Temperatura max futura']))
